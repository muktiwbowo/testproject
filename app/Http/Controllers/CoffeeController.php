<?php

namespace App\Http\Controllers;

use App\Helpers\ResponseFormatter;
use App\Models\Coffee;
use Illuminate\Http\Request;

class CoffeeController extends Controller
{
    public function all(Request $request){
        $id = $request->input('id');
        $name = $request->input('name');
        $price = $request->input('price');
        $limit = $request->input('limit', 6);

        if($id){
            $coffee = Coffee::find($id);

            if($coffee){
                return ResponseFormatter::success(
                    $coffee, 'Data successfully fetch'
                );
            }else{
                return ResponseFormatter::error(
                    'error', 'Data not found', 404
                );
            }
        }

        $coffee = Coffee::query();
        if($name){
            $coffee->where('name', 'like', '%' . $name . '%');
        }

        if($price){
            $coffee->where('price', 'like', '%' . $price . '%');
        }

        return ResponseFormatter::success(
            $coffee->paginate($limit), 'Data coffee successfully fetched'
        );
    }
}
