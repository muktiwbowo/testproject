<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Helpers\ResponseFormatter;
use App\Http\Controllers\Controller;
use App\Models\Transaction;
use Illuminate\Support\Facades\Auth;

class TransactionController extends Controller
{
    public function all(Request $request){
        $id = $request->input('id');
        $coffee_id = $request->input('coffee_id');
        $status = $request->input('status');
        $limit = $request->input('limit', 6);

        if($id){
            $transaction = Transaction::with(['coffee', 'user'])->find($id);

            if($transaction){
                return ResponseFormatter::success(
                    $transaction, 'Data transaction successfully fetch'
                );
            }else{
                return ResponseFormatter::error(
                    'error', 'Data not found', 404
                );
            }
        }

        $transaction = Transaction::with(['coffee', 'user'])->where('user_id', Auth::user()->id);

        if($coffee_id){
            $transaction->where('coffee_id', $coffee_id);
        }

        if($status){
            $transaction->where('status', $status);
        }

        return ResponseFormatter::success(
            $transaction->paginate($limit), 'Data transaction successfully fetched'
        );
    }

    public function update(Request $request, $id){
        $transaction = Transaction::findOrFail($id);
        $transaction->update($request->all());
        return ResponseFormatter::success(
            $transaction, 'Transaction updated'
        );
    }
}
